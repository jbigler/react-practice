import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const Card = (props) => {
    return (
        <div className="Box" style={{margin: "1em"}}>
            <img className="cardText" width='75' src={props.avatar_url} />
            <div  className="cardText" style={{display: 'inline-block', marginLeft:10, marginBottom:30}}>
                <div className="cardBody" style={{fontSize: '1.25em', fontWeight: 'bold'}}>
                    {props.name}
                </div>
                <div>{props.company}</div>
            </div>
        </div>

    )
}

/* const Cardlist = function (props) {   */
const CardList = (props) => {
    return (
        <div>
            {props.cards.map(card => <Card key={card.id}{...card} />)}    

        </div>
    );
}

class Form extends React.Component {
    state = { userName: ''}
    handleSubmit = (event) => {
        event.preventDefault();
        console.log('Event: Form Submit', this.state.userName);
        fetch(`https://api.github.com/users/${this.state.userName}`)
            .then((response) => response.json())
            .then((responseJson) => {
                this.props.onSubmit(responseJson)
        })
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" 
                    value={this.state.userName}
                    onChange={(event) => this.setState({ userName: event.target.value})}
                    placeholder="Github username" required/>
                <button type="submit">Add card</button>
            </form>

        )
    }
}

class App extends React.Component {
    state = {
        cards: [
            {
                id:2,
                name:"Paul O'Shannessy",
                avatar_url: "https://avatars.githubusercontent.com/u/8445?v=3",
                company:"Facebook"},

            {
                id:1,
                name:"Ben Alpert",
                avatar_url: "https://avatars.githubusercontent.com/u/6820?v=3",
                company: "Facebook"
            },
        ]
    };

    addNewCard = (cardInfo) => {
        console.log(cardInfo)
        this.setState(prevState => ({
            cards: prevState.cards.concat(cardInfo)
        }));
    };


    render() {
        return (
            <div>
                <Form onSubmit={this.addNewCard} />
                <CardList cards={this.state.cards} />
            </div>
        );
    }
}


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
